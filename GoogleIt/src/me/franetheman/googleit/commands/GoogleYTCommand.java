package me.franetheman.googleit.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.franetheman.googleit.UtilMessage;
import me.franetheman.googleit.jsonchat.JSONClickEvent;
import me.franetheman.googleit.jsonchat.JSONColor;
import me.franetheman.googleit.jsonchat.JSONMessage;

public class GoogleYTCommand implements CommandExecutor {

	private final String SEARCH_ADDRESS = "https://www.youtube.com/results?search_query=";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender.hasPermission("googleit.googleyt")) {

			if (args.length == 0) {
				sender.sendMessage("/googleyt <search keywords>");
			} else {

				String search = "";

				for (int i = 0; i < args.length; i++) {
					search = search + args[i] + " ";
				}

				String message = new JSONMessage("Click here for your google results").withBold()
						.withColor(JSONColor.AQUA).withClick(JSONClickEvent.OPEN_URL, SEARCH_ADDRESS + search.replace(' ', '+')).build();

				UtilMessage.sendChatPacket((Player)sender, message);
			}

		}

		return false;
	}

}
