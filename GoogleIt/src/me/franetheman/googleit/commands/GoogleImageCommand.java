package me.franetheman.googleit.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.franetheman.googleit.UtilMessage;
import me.franetheman.googleit.jsonchat.JSONClickEvent;
import me.franetheman.googleit.jsonchat.JSONColor;
import me.franetheman.googleit.jsonchat.JSONMessage;

public class GoogleImageCommand implements CommandExecutor {

	private final String SEARCH_ADDRESS = "https://www.google.com/#q=";
	private final String SEARCH_SUFFIX = "&biw=1920&bih=969&source=lnms&tbm=isch&sa=X&ei=fpVXVci-KrWJsQSF-YCoDQ&ved=0CAYQ_AUoAQ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender.hasPermission("googleit.googleimg")) {

			if (args.length == 0) {
				sender.sendMessage("/googleimg <search keywords>");
			} else {

				String search = "";

				for (int i = 0; i < args.length; i++) {
					search = search + args[i] + " ";
				}

				String message = new JSONMessage("Click here for your google results").withBold()
						.withColor(JSONColor.AQUA).withClick(JSONClickEvent.OPEN_URL, SEARCH_ADDRESS + search.replace(' ', '+') + SEARCH_SUFFIX).build();

				UtilMessage.sendChatPacket((Player)sender, message);

			}

		}

		return false;
	}
}
