package me.franetheman.googleit.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.franetheman.googleit.UtilMessage;
import me.franetheman.googleit.jsonchat.JSONClickEvent;
import me.franetheman.googleit.jsonchat.JSONColor;
import me.franetheman.googleit.jsonchat.JSONMessage;

public class GoogleVideoCommand implements CommandExecutor {

	private final String SEARCH_ADDRESS = "https://www.google.com/#q=";
	private final String SEARCH_SUFFIX = "&tbm=vid";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender.hasPermission("googleit.googlevid")) {

			if (args.length == 0) {
				sender.sendMessage("/googlevid <search keywords>");
			} else {

				String search = "";

				for (int i = 0; i < args.length; i++) {
					search = search + args[i] + " ";
				}

				String message = new JSONMessage("Click here for your google results").withBold()
						.withColor(JSONColor.AQUA).withClick(JSONClickEvent.OPEN_URL, SEARCH_ADDRESS + search.replace(' ', '+') + SEARCH_SUFFIX).build();

				UtilMessage.sendChatPacket((Player)sender, message);
			}

		}

		return false;
	}
}
