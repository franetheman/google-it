package me.franetheman.googleit.jsonchat;

public enum JSONClickEvent {

	RUN_COMMAND("run_command"), SUGGEST_COMMAND("suggest_command"), OPEN_URL("open_url"), CHANGE_PAGE("change_page");

	private String _clickEventString;

	private JSONClickEvent(String clickEventString) {
		_clickEventString = clickEventString;
	}

	public String getClickEventString() {
		return _clickEventString;
	}

}
