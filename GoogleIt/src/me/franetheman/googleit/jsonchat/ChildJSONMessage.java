package me.franetheman.googleit.jsonchat;

public class ChildJSONMessage extends JSONMessage {

	private JSONMessage _parent;

	public ChildJSONMessage(String text) {
		this(new StringBuilder(), text);
	}

	public ChildJSONMessage(StringBuilder stringBuilder, String text) {
		this(null, stringBuilder, text);
	}

	public ChildJSONMessage(JSONMessage parent, StringBuilder stringBuilder, String text) {
		super(text, stringBuilder);
		_parent = parent;
	}

	@Override
	public ChildJSONMessage withColor(JSONColor color) {
		super.withColor(color);
		return this;
	}

	@Override
	public ChildJSONMessage withBold() {
		super.withBold();
		return this;
	}

	@Override
	public ChildJSONMessage withItalic() {
		super.withItalic();
		return this;
	}

	@Override
	public ChildJSONMessage withUnderline() {
		super.withUnderline();
		return this;
	}

	@Override
	public ChildJSONMessage withStrikethrough() {
		super.withStrikethrough();
		return this;
	}

	@Override
	public ChildJSONMessage withObfuscation() {
		super.withObfuscation();
		return this;
	}

	@Override
	public ChildJSONMessage withClick(JSONClickEvent clickEvent, String value) {
		super.withClick(clickEvent, value);
		return this;
	}

	@Override
	public ChildJSONMessage withHover(JSONHoverEvent hoverEvent, String value) {
		super.withHover(hoverEvent, value);
		return this;
	}

	@Override
	public String build() {
		_stringBuilder.append("}");
		if (_parent != null) {
			_stringBuilder.append("]");
			return _parent.build();
		} else {
			return _stringBuilder.toString();
		}
	}

}
