package me.franetheman.googleit.jsonchat;

public enum JSONHoverEvent {

	SHOW_TEXT("show_text"), SHOW_ITEM("show_item"), SHOW_ACHIEVEMENT("show_achievement");

	private String _hoverEventString;

	private JSONHoverEvent(String hoverEventString) {
		_hoverEventString = hoverEventString;
	}

	public String getHoverEventString() {
		return _hoverEventString;
	}

}
