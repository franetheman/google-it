package me.franetheman.googleit.jsonchat;

public class JSONMessage {

	protected StringBuilder _stringBuilder;

	public JSONMessage(String text) {
		this(text, new StringBuilder());
	}

	public JSONMessage(String text, StringBuilder stringBuilder) {
		_stringBuilder = stringBuilder;
		_stringBuilder.append("{\"text\":\"" + text + "\"");
	}

	public JSONMessage withColor(JSONColor color) {
		_stringBuilder.append(", color:" + color.getColorString());
		return this;
	}

	public JSONMessage withBold() {
		_stringBuilder.append(", bold:true");
		return this;
	}

	public JSONMessage withItalic() {
		_stringBuilder.append(", italic:true");
		return this;
	}

	public JSONMessage withUnderline() {
		_stringBuilder.append(", underlined:true");
		return this;
	}

	public JSONMessage withStrikethrough() {
		_stringBuilder.append(", strikethrough:true");
		return this;
	}

	public JSONMessage withObfuscation() {
		_stringBuilder.append(", obfuscated:true");
		return this;
	}

	public ChildJSONMessage withExtra(String text) {
		_stringBuilder.append(", \"extra\":[");
		return new ChildJSONMessage(this, _stringBuilder, text);
	}

	public JSONMessage withClick(JSONClickEvent clickEvent, String value) {
		_stringBuilder.append(", \"clickEvent\":{\"action\":\"" + clickEvent.getClickEventString() + "\",\"value\":\""
				+ value + "\"}");
		return this;
	}

	public JSONMessage withHover(JSONHoverEvent hoverEvent, String value) {
		_stringBuilder.append(", \"hoverEvent\":{\"action\":\"" + hoverEvent.getHoverEventString() + "\",\"value\":\""
				+ value + "\"}");
		return this;
	}

	public String build() {
		return _stringBuilder.append("}").toString();
	}

}
