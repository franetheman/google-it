package me.franetheman.googleit;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;

public final class UtilMessage {

	public static void sendChatPacket(Player player, String message)
	{
		((CraftPlayer) player).getHandle().
			playerConnection.sendPacket(new PacketPlayOutChat
			(ChatSerializer.a(
			message
			)));
	}
	
	private UtilMessage(){}
}
